#include <minix/drivers.h>
#include <minix/chardriver.h>
#include <stdlib.h>
#include <minix/ds.h>
#include <minix/ioctl.h>
#include <sys/ioc_dfa.h>

/* Constants */
#define NODES_NR        256
#define ALPHABET_SIZE   256
#define BUFF_MAX        256

/* Function prototypes for the dfa driver. */
static int dfa_open(devminor_t minor, int access, endpoint_t user_endpt);
static int dfa_close(devminor_t minor);
static ssize_t dfa_read(devminor_t minor, u64_t position, endpoint_t endpt,
        cp_grant_id_t grant, size_t size, int flags, cdev_id_t id);
static ssize_t dfa_write(devminor_t minor, u64_t position, endpoint_t endpt,
        cp_grant_id_t grant, size_t size, int flags, cdev_id_t id);
static int dfa_ioctl(devminor_t minor, unsigned long request, endpoint_t endpt,
        cp_grant_id_t grant, int flags, endpoint_t user_endpt, cdev_id_t id);

/* SEF functions and variables. */
static void sef_local_startup(void);
static int sef_cb_init(int type, sef_init_info_t *info);
static int sef_cb_lu_state_save(int);
static int lu_state_restore(void);

/* Entry points to the hello driver. */
static struct chardriver dfa_tab =
{
    .cdr_open	= dfa_open,
    .cdr_close	= dfa_close,
    .cdr_read	= dfa_read,
    .cdr_write  = dfa_write,
    .cdr_ioctl  = dfa_ioctl
};

/* Variables */
unsigned char currNode;
unsigned char final[NODES_NR];
unsigned char function[NODES_NR * ALPHABET_SIZE];

static int dfa_open(devminor_t UNUSED(minor), int UNUSED(access),
    endpoint_t UNUSED(user_endpt))
{
    return OK;
}

static int dfa_close(devminor_t UNUSED(minor))
{
    return OK;
}

static ssize_t dfa_read(devminor_t UNUSED(minor), u64_t position,
    endpoint_t endpt, cp_grant_id_t grant, size_t size, int UNUSED(flags),
    cdev_id_t UNUSED(id))
{
    unsigned char word[BUFF_MAX];
    int nrbytes = size;
    int chunk;

    int ret;

    if (final[currNode] == 1)
        memset(word, 'Y', BUFF_MAX);
    else
        memset(word, 'N', BUFF_MAX);

    while (nrbytes > 0) {
        chunk = (nrbytes > BUFF_MAX) ? BUFF_MAX : nrbytes;
        if ((ret = sys_safecopyto(endpt, grant, 0, (vir_bytes) word, chunk)) != OK)
            return ret;

        nrbytes -= chunk;
    }

    /* Return the number of bytes read. */
    return size;
}

static ssize_t dfa_write(devminor_t UNUSED(minor), u64_t position,
    endpoint_t endpt, cp_grant_id_t grant, size_t size, int UNUSED(flags),
    cdev_id_t UNUSED(id))
{
    unsigned char word[BUFF_MAX];
    int nrbytes = size;
    int chunk;

    int ret;

    while (nrbytes > 0) {
        chunk = (nrbytes > BUFF_MAX) ? BUFF_MAX : nrbytes;
        if ((ret = sys_safecopyfrom(endpt, grant, 0, (vir_bytes) word, chunk)) != OK)
            return ret;

        for (int i = 0; i < chunk; ++i)
            currNode = function[ALPHABET_SIZE*currNode + word[i]];

        nrbytes -= chunk;
    }

    /* Return the number of bytes writen. */
    return size;
}

static int dfa_ioctl(devminor_t UNUSED(minor), unsigned long request, endpoint_t endpt,
    cp_grant_id_t grant, int UNUSED(flags), endpoint_t user_endpt, cdev_id_t UNUSED(id)) {
    int ret;
    unsigned char transition[3];
    unsigned char state;

    switch(request) {
        case DFAIOCRESET:
            currNode = 0;
            return OK;

        case DFAIOCADD:
            ret = sys_safecopyfrom(endpt, grant, 0, (vir_bytes) transition, 3);
            if (ret != OK)
                return ret;

            function[ALPHABET_SIZE*transition[0] + transition[1]] = transition[2];
            currNode = 0;
            return OK;

        case DFAIOCACCEPT:
            ret = sys_safecopyfrom(endpt, grant, 0, (vir_bytes) &state, 1);
            if (ret != OK)
                return ret;

            final[state] = 1;
            return OK;

        case DFAIOCREJECT:
            ret = sys_safecopyfrom(endpt, grant, 0, (vir_bytes) &state, 1);
            if (ret != OK)
                return ret;

            final[state] = 0;
            return OK;
    }

    return ENOTTY;
}

static int sef_cb_lu_state_save(int UNUSED(state)) {
    /* Save the state. */
    ds_publish_mem("currNode", &currNode, 1, DSF_OVERWRITE);
    ds_publish_mem("final", final, NODES_NR, DSF_OVERWRITE);
    ds_publish_mem("function", function, NODES_NR*ALPHABET_SIZE, DSF_OVERWRITE);

    return OK;
}

static int lu_state_restore() {
    /* Restore the state. */
    size_t len;

    len = 1;
    ds_retrieve_mem("currNode", &currNode, &len);
    ds_delete_mem("currNode");

    len = NODES_NR;
    ds_retrieve_mem("final", final, &len);
    ds_delete_mem("final");

    len = NODES_NR*ALPHABET_SIZE;
    ds_retrieve_mem("function", function, &len);
    ds_delete_mem("function");

    return OK;
}

static void sef_local_startup()
{
    /* Register init callbacks. Use the same function for all event types. */
    sef_setcb_init_fresh(sef_cb_init);
    sef_setcb_init_lu(sef_cb_init);
    sef_setcb_init_restart(sef_cb_init);

    /* Register live update callbacks. */
    /* - Agree to update immediately when LU is requested in a valid state. */
    sef_setcb_lu_prepare(sef_cb_lu_prepare_always_ready);
    /* - Support live update starting from any standard state. */
    sef_setcb_lu_state_isvalid(sef_cb_lu_state_isvalid_standard);
    /* - Register a custom routine to save the state. */
    sef_setcb_lu_state_save(sef_cb_lu_state_save);

    /* Let SEF perform startup. */
    sef_startup();
}

static int sef_cb_init(int type, sef_init_info_t *UNUSED(info)) {
    /* Initialize the dfa driver. */
    int do_announce_driver = TRUE;

    switch(type) {
        case SEF_INIT_FRESH:
            currNode = 0;
            memset(final, 0, NODES_NR);
            memset(function, 0, NODES_NR*NODES_NR);
            break;

        case SEF_INIT_LU:
            /* Restore the state. */
            lu_state_restore();
            do_announce_driver = FALSE;
            break;

        case SEF_INIT_RESTART:
            break;
    }

    /* Announce we are up when necessary. */
    if (do_announce_driver)
        chardriver_announce();

    /* Initialization completed successfully. */
    return OK;
}

int main(void) {
    /* Perform initialization. */
    sef_local_startup();

    /* Run the main loop. */
    chardriver_task(&dfa_tab);
    return OK;
}