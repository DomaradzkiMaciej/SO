global _start

SYS_EXIT   equ 60
SYS_READ   equ 0
SYS_WRITE  equ 1
STDOUT     equ 1
STDIN      equ 0
NULL       equ 0
NCHARS     equ 42
BUF_SIZE   equ 4096

%define L   r13
%define R   r14
%define T   r15
%define lr  r11
%define l   r10b
%define r   r12b
%define cur bl

%macro QTransition 1
         add cur, %1
         cmp cur, NCHARS
         jb  %%after_sub
         sub cur, NCHARS
%%after_sub:
%endmacro

%macro ReverseQTransition 1
         test %1, %1
         je %%equal
         mov cl, 42
         sub cl, %1
         jmp %%afterTest
%%equal:
         mov cl, 0

%%afterTest:
         add cur, cl
         cmp cur, NCHARS
         jb  %%after_sub
         sub cur, NCHARS
%%after_sub:
%endmacro


section .bss
  LInv:    resb NCHARS
  RInv:    resb NCHARS
  TInv:    resb NCHARS
  buffer:  resb BUF_SIZE


section .text
normalize_and_verify:
         lea     r8, [rdi+NCHARS]             ; zapisz do r8 adres pierwszego parametru + liczba znaków
         mov     rdx, rdi                     ; zapisz pod rdx adres pierwszego parametru
         jmp     condition1

normalize_loop:
         mov     BYTE [rdx], al               ; zapisz znak zmniejszony o 49
         movzx   eax, al                      ; rozszerz obecny znak (pomniejszony o 49)
         add     rax, rsi                     ; dodaj do drugiego parametru obecnie przetwarzany znak (pomniejszony o 49)
         cmp     BYTE [rax], NULL             ; sprawdź, czy pod tym miejscem w pamięci jest już coś zapisane
         jne     exit_wrong                   ; jeśli jest, to zakończ program
         mov     ecx, edx                     ; przenieś od ecx wskaźnik na aktualny znak (dokładniej jego prawą połówkę)
         add     rdx, 1                       ; przejdź do kolejnego znaku
         sub     ecx, edi                     ; odejmnij od ecx adres pierszego znaku (otrzymamy w ten sposób iterację pętli)
         cmp     rdx, r8                      ; sprawdź, czy jesteśmy już na 42 znaku
         mov     BYTE [rax], cl               ; zapisz do inv[*buf] iterację pętli
         je      after_loop                   ; jeśli tak, to zakończ pętlę

condition1:
         mov     al, BYTE [rdx]               ; zapisz do al znak, na który wskazuje rdx
         sub     al, '1'                      ; odejmnij od al 49 (znak 1 w ASCII), jeżeli al < 49, to al się przekręci
         cmp     al, 41                       ; porównaj al z 41 (41 + 49 to znak Z w ASCII)
         jbe     normalize_loop               ; skocz do pętli, jeśli znak mieści się w zakresie
         jmp     exit_wrong                   ; jeśli nie, zakończ program

after_loop:
         cmp     BYTE [rdx], NULL             ; sprawdź, czy na 43 znaku jest koniec linii
         ja      exit_wrong                   ; jeśli nie, to zakończ program
         ret


_start:
;parsowanie argumentów wywołań programu
         cmp     dword [rsp], 5               ; sprawdź, czy liczba argumentów wynosi 5 (razem z nazwą programu)
         jne     exit_wrong                   ; jeśli nie, to skocz do zakończenia programu

         mov     L, [rsp+16]                  ; zapisz wskaźniki na odpowiednie parametry
         mov     R, [rsp+24]
         mov     T, [rsp+32]
         mov     lr, [rsp+40]

         mov     l, BYTE[lr]
         sub     l, '1'                       ; odejmnij od l 49 (znak 1 w ASCII), jeżeli l < 49, to l się przekręci
         cmp     l, 41                        ; porównaj l z 41 (41 + 49 to znak Z w ASCII)
         ja      exit_wrong                   ; jeśli znak jest spoza zakresu (l > 41), zakończ program

         mov     r, BYTE[lr+1]
         sub     r, '1'                       ; odejmnij od r 49 (znak 1 w ASCII), jeżeli r < 49, to r się przekręci
         cmp     l, 41                        ; porównaj r z 41 (41 + 49 to znak Z w ASCII)
         ja      exit_wrong                   ; jeśli znak jest spoza zakresu (r > 41), zakończ program

         cmp     BYTE [lr+2], NULL            ; sprawdź, czy 5 parametr zawiera tylko 2 znaki (czy po dwóch znakach jest NULL)
         jne     exit_wrong                   ; jeśli nie, zakończ program

         mov     rdi, L                       ; sprawdź poprawność permutacji L i oblicz permutację przeciwną
         mov     rsi, LInv
         call    normalize_and_verify

         mov     rdi, R                       ; sprawdź poprawność permutacji R i oblicz permutację przeciwną
         mov     rsi, RInv
         call    normalize_and_verify

         mov     rdi, T                       ; sprawdź poprawność permutacji T i oblicz permutację przeciwną
         mov     rsi, TInv
         call    normalize_and_verify

         xor     rbx, rbx                     ; zainicjuj i na 0
loopT:
         mov     r9b, BYTE [TInv+rbx]
         cmp     r9b, BYTE [T+rbx]            ; sprawdź czy na pozycji i w obu tablicach jest ten sam znak
         jne     exit_wrong                   ; jeśli nie, zakończ program
         add     rbx, 1                       ; ++i
         cmp     rbx, NCHARS
         jne     loopT                        ; jeśli i < 42, powtórz pętlę


;wczytywanie i szyfrowanie/deszyfrowanie
outer_loop:
         mov     eax, SYS_READ
         mov     edi, STDIN
         mov     rsi, buffer
         mov     edx, BUF_SIZE
         syscall                              ; wczytaj ze standardowego wejścia do bufora max BUF_SIZE znaków

         test    eax, eax
         jl      exit_wrong                   ; wartość ujemna - błąd w czytaniu
         je      exit_ok                      ; 0 - skończenie czytania

         mov     r8, buffer
         lea     r9, [buffer+eax]
inner_loop:
         mov     cur, BYTE[r8]                ; zapisz do r10b obecnie przetwarzany znak
         sub     cur, '1'                     ; odejmnij od cur 49 (znak 1 w ASCII), jeżeli cur < 49, to cur się przekręci
         cmp     cur, 41                      ; porównaj cur z 41 (41 + 49 to znak Z w ASCII)
         ja      exit_wrong                   ; jeśli znak jest spoza zakresu (bl > 41), zakończ program

         add     r, 1                         ; zwiększ r
         cmp     r, 42                        ; sprawdź, czy r == 42
         jne     check_whether_increase_l     ; jeśli nie, powiń kolejną instrukcję
         mov     r, 0                         ; a jeśli tak, to ustaw r na 0

check_whether_increase_l:
         cmp     r, 27                        ; 27 = 'L' - '1'
         je      increment_l
         cmp     r, 33                        ; 33 = 'R' - '1'
         je      increment_l
         cmp     r, 35                        ; 35 = 'T' - '1'
         jne     after_increment_l

increment_l:
         add     l, 1                         ; zwiększ l
         cmp     l, 42                        ; sprawdź, czy l == 42
         jne     after_increment_l            ; jeśli nie, pomiń kolejną instrukcję
         mov     l, 0                         ; a jeśli tak, to ustaw l na 0

after_increment_l:
         QTransition r
         movzx rbx, cur
         mov cur, [R+rbx]
         ReverseQTransition r


         QTransition l
         movzx rbx, cur
         mov cur, [L+rbx]
         ReverseQTransition l

         movzx rbx, cur
         mov cur, [TInv+rbx]

         QTransition l
         movzx rbx, cur
         mov cur, [LInv+rbx]
         ReverseQTransition l

         QTransition r
         movzx rbx, cur
         mov cur, [RInv+rbx]
         ReverseQTransition r

         add     cur, '1'                     ; dodaj do cur 49 (znak 1 w ASCII) przywracająć mu poprawną wartość w ASCII
         mov     BYTE[r8], cur                ; zapisz zaszyfrowany znak na odpowiednie miejsce w buforze
inner_loop_condition:
         add     r8, 1                        ; przejdź do kolejnego znaku
         cmp     r8, r9                       ; sprawdź, czy wyszliśmy poza zakres
         jb      inner_loop                   ; jeśli nie, powtórz pętlę

         mov     edx, eax                     ; wypisujemy tyle ile wczytaliśmy
         mov     eax, SYS_WRITE
         mov     edi, STDOUT
         mov     rsi, buffer
         syscall                              ; wypisujemy zaszyfrowane znaki

         test    eax, eax
         jl      exit_wrong
         jmp     outer_loop                   ; czytamy kolejne porcje do bufora


exit_ok:
         mov     eax, SYS_EXIT
         xor     edi, edi
         syscall

exit_wrong:
         mov     eax, SYS_EXIT
         mov     edi, 1
         syscall
