# New Minix System Calls

## Introduction

Minix patch that adds new system calls:

1. PM_CHANGE_PARENT with the library function int changeparent () - The PM_CHANGE_PARENT function changes the parent of the process that called the function to his "grandfather", i.e. the parent of the existing parent, provided that:
- current parent is not an init process and
- the parent does not wait for the child to finish, i.e. does not execute the wait () function.

2. PM_GETOPPID with the library function pid_t getoppid (pid_t pid) - the function passes as a result the identifier of the original parent for the given process

