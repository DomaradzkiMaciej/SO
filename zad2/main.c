#include "main.h"
#include <stdio.h>

void pixtime(uint64_t clock_tick) {
    fprintf(stderr, "%016lX\n", clock_tick);
}

int main() {
    const int N = 256;
    uint32_t table[N];
    uint64_t index = 0;
    pix(table, &index, N);

    for (int i = 0; i < N; i++) {
        printf("%08X\n", table[i]);
    }

    return 0;
}
