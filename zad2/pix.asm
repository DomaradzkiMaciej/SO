global pix
extern pixtime

%define  ppi           r12
%define  pidx          r13
%define  max           r14
%define  current       r8
%define  j             r9
%define  result        r10
%define  res           r11
%define  k             r15
%define  numerator     rsi
%define  denominator   rcx
%define  curPart       rbx
%define  aux           rdi
%define  oneSixteenth  rdi
%define  number        rbp
%define  l             rbp
%define  macroRes      rax

%macro callPixTime 0
         rdtsc                                   ; pod edx:eax zostaje zapisana liczba cykli zegara
         shl     rdx, 32                         ; przesuwamy górną połówkę liczby cykli zegara ze starszej połowy rdx do młodszej
         or      rdx, rax                        ; scalamy liczbę cykli zegara do jednego rejestru
         mov     rdi, rdx                        ; zapisujemy ją w rdi (gdyż będziemy ją potrzebowali do zawołania pixtime)
         call    pixtime
%endmacro

%macro getDivFraction1 2
         mov     rax, %1                         ; przenosimy pierwszy parametr do rax
         xor     rdx, rdx                        ; zerujemy rdx
         div     %2                              ; liczymy %1/%2
         mov     %1, rdx                         ; %1 = (%1) % (%2)

         mov     rdx, %1                         ; przenosimy pierwszy parametr do rdx
         xor     rax, rax                        ; zerujemy rax
         div     %2                              ; dzielimy rdx:rax przez drugi parametr
%endmacro

%macro getOneSixteenth 0
         mov     number, 16
         mov     rdx, 1                          ; przenosimy pierwszy parametr do rdx
         xor     rax, rax                        ; zerujemy rax
         div     number                          ; dzielimy rdx:rax przez drugi parametr
%endmacro

%macro getDenominator 0
         mov     rax, k                          ; rax = k
         mov     number, 8
         mul     number                          ; rax = 8*k
         mov     denominator, rax                ; denominator = 8*k
         add     denominator, j                  ; denominator = 8*k + j
%endmacro

%macro getNumerator 2
         xor     l, l                            ; zerujemy l
         mov     macroRes, 1

         cmp     %2, 1
         jne     %%afterCompire
         mov     macroRes, 0

%%afterCompire:
         jmp     %%macroCon                      ; skocz do warunku pętli

%%macroLoop:
         mov     rdx, 16
         mul     rdx                             ; mnożymy macroRes(rax) przez 16

         div     %2                              ; dzielimy wynik przez %2
         mov     macroRes, rdx                   ; zapisujemy resztę z dzielenia do macroRes

         add     l, 1

%%macroCon:
         cmp     l, %1                           ; sprawdzamy, czy jesteśmy jeszcze w zakresie pętli
         jb      %%macroLoop                     ; jeśli tak, to skaczemy do pętli

         mov     numerator, macroRes
%endmacro


section .text

getSjForN:
         xor     res, res                        ; zerujemy res
         xor     k, k                            ; podobnie k

         jmp     con1                            ; skaczemy do warunku pętli
loop1:
         mov     aux, current                    ; aux = current
         sub     aux, k                          ; aux = current - k

         getDenominator
         getNumerator  aux, denominator
         getDivFraction1 numerator, denominator

         add     res, rax                        ; res += {numerator/denominator}
         add     k, 1
con1:
         cmp     k, current                      ; sprawdzamy czy jesteśmy w zakresie pętli
         jbe     loop1                           ; jeśli k <= current wykonaj obrót pętli

         getOneSixteenth                         ; obliczamy {1/16}
         mov oneSixteenth, rax                   ; oneSixteenth = {1/16}
         mov numerator, rax                      ; oneSixteenth = {1/16}
loop2:
         getDenominator

         xor     rdx, rdx                        ; zerujemy rdx
         mov     rax, numerator                  ; zapisujemy numerator do rax
         div     denominator                     ; dzielimy rdx:rax przez denominator
         mov     curPart, rax                    ; zapisujemy do rdx floor(numerator2/denominator)

         test    curPart, curPart                ; sprawdzamy, czy curPart == 0
         je      afterLoop2                      ; jeśli tak, to kończymy pętlę

         add     res, curPart                    ; res += curPart

         mov     rax, numerator                  ; zapisujemy numerator do rax
         mul     oneSixteenth                    ; rdx:rax = oneSixteenth * numerator2
         mov     numerator, rdx                  ; numerator = (oneSixteenth * numerator) >> 32

         add     k, 1
         jmp     loop2                           ; powtarzamy pętlę

afterLoop2:
         mov     rax, res                        ; zapisz wynik do rax

         ret


;udostępniana na zewnątrz funkcja
pix:
         push    rbx                             ; zachowujemy rejestry
         push    rbp
         push    r12
         push    r13
         push    r14
         push    r15

         sub     rsp, 8                          ; wyrównujemy stos

         mov     ppi, rdi                        ; zapisujemy argumenty do callee-saved registers
         mov     pidx, rsi
         mov     max, rdx

         callPixTime

         jmp     condition                       ; skocz do warunku pętli
loop:
         shl     current, 3                      ; liczymy od razu 8 cyfr rozwinięcia szesnastkowego

         mov     j, 1
         call    getSjForN                       ; obliczamy Sj dla j = 1
         mov     number, 4
         mul     number                          ; mnożymy wynik przez 4
         mov     result, rax                     ; zapisujemy go do result

         mov     j, 4
         call    getSjForN                       ; obliczamy Sj dla j = 4
         mov     number, 2
         mul     number                          ; mnożymy wynik przez 2
         sub     result, rax                     ; odejmujemy go od result

         mov     j, 5
         call    getSjForN                       ; obliczamy Sj dla j = 5
         sub     result, rax                     ; odejmujemy go od result

         mov     j, 6
         call    getSjForN                       ; obliczamy Sj dla j = 5
         sub     result,rax                      ; odejmujemy go od result

         shr     result, 32                      ; zapisujemy 32 najbardzej znaczące bity
         shr     current, 1                      ; current /= 2

         mov     DWORD[ppi + current], r10d      ; zapisujemy obliczoną wartość pod odpowiednie miejsce w tablicy

condition:
         mov     current, 1
         lock \
         xadd    QWORD[pidx], current            ; dodaj 1 do *pidx, a poprzednią wartość zapisz do current
         cmp     current, max                    ; sprawdź, czy jesteśmy jeszcze w zadanym zakresie
         jb      loop                            ; jeśli tak, to skocz do pętli

         callPixTime

         add     rsp, 8                          ; na początku zostało odjęte od rsp 8, aby wyrównać stos

         pop     r15                             ; przywracamy rejestry
         pop     r14
         pop     r13
         pop     r12
         pop     rbp
         pop     rbx

         ret                                     ; kończymy działanie funkcji
