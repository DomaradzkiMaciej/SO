# DCL

## Introduction

Program that simulates the operation of a DCL cipher machine. The DCL machine operates on a set of acceptable characters containing: upper case letters of the English alphabet, numbers 1 to 9, a colon, a semicolon, a question mark, an equal sign, a minor sign, a majority sign, an at sign. Only characters from this set can appear in the correct program parameters and in the correct program entry and exit.

