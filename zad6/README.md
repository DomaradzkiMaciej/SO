# Deterministic Finite-state Automaton

## Introduction

A driver for the /dev/dfa character device that simulates a deterministic finite automaton.

## Usage

The operation of reading from the device gives information about whether the word loaded so far belongs to a language recognized by the machine. More precisely, an attempt to read n bytes gives n bytes equal to 89 if the word belongs to a language, or n bytes equal to 78 otherwise.

The write operation allows entering the next bytes of the recognized word.

In addition to supporting reading and writing operations, the driver also implements an ioctl operation that allows the following commands to be performed:
- DFAIOCRESET - resets the currently read word,
- DFAIOCADD - passes char\[3] structure corresponding to trio ⟨p, a, p′⟩ and changes the current function of the automaton δ to the function δ′ given by the equation δ′ (q,c) = {p′ if⟨q,c⟩ = ⟨p, a⟩, δ (q, c) else. In addition, this command resets the currently read word as above.
- DFAIOCACCEPT - passes a variable of type char corresponding to the state p and modifies the set of machine accepting states by adding p state to it. This command does not reset the currently read word. It doesn't matter if p belonged to the set of accepting states before calling the command.
- DFAIOCREJECT - it works similarly to DFAIOCACCEPT, except that the passed state p becomes an unacceptable state.

