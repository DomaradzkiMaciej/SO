# File Encryption

## Introduction

Minix patch that adds file encryption.

## Usage

The files are encrypted with a single-byte key. The key is given by writing to the KEY file in the root directory of the partition (this value is not actually saved in the file content). 
Immediately after mounting the partition is locked. 
If the user does not want to encrypt the partition, a file or directory named NOT_ENCRYPTED can be placed in its root directory.

