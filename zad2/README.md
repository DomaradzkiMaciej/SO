# PIX

## Introduction

A function that calculates the selected fragment of the fractional part of the binary expansion of π and which can be called from the C language.
